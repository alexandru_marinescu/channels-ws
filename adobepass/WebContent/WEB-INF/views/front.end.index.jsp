<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page session="false" %>
<html>
	<head>
			<title>Adobe Pass</title>
			<link rel="stylesheet/less" type="text/css" href="<c:url value="/static/css/main.lss" />" />
			<script type="text/javascript" src="<c:url value="/static/js/lib/less-1.3.0.min.js" />"></script>
			<script type="text/javascript" src="<c:url value="/static/js/lib/jquery.min.js" />"></script>
			<script type="text/javascript" src="<c:url value="/static/js/lib/jquery.ui.widget.js" />"></script>
			<script type="text/javascript" src="<c:url value="/static/js/lib/jquery.fileupload.js" />"></script>
			<script type="text/javascript" src="<c:url value="/static/js/app/adobe.pass.js" />"></script>
	</head>
	<body>
		<div class="main-container">
			<div class="rounded-container" id="channels-container">
				<span id="available-info">Available channels</span>
				<ul id="available-channels"></ul>
			</div>
			<div class="rounded-container" id="control-panel">
				<p>Tune TV Commercial parameters</p>
				<div class="center">
					<div class="in-center">
						<div class="input-container">
							<label for="from-date"><span class="gray-info">starting </span>from</label>
							<input id="from-date" type="text" class="from-date" />				
						</div>
						<div class="input-container small">
							<label for="occurences"><span class="gray-info">number of </span>occurences</label>
							<input type="text" id="occurences" class="occurences" />					
						</div>
						<div class="input-container">
							<label for="to-date"><span class="gray-info">ending </span>on</label>
							<input type="text" id="to-date" class="to-date" />
						</div>
						<div class="input-container small">
							<label for="duration">duration <span class="gray-info">in seconds</span></label>
							<input type="text" id="duration" class="duration" />					
						</div>
						<div class="input-container-clear">
							<a class="button" id="get-prices-button">Get prices</a>
						</div>
					</div>
				</div>
			</div>
			<div class="rounded-container" id="prices-container">
				<table>
					<thead id="price-table-head">
						<tr>
							<td class="dashed-border">Channel name</td>
							<td>Price</td>
						</tr>
					</thead>
					<tbody id="price-table-body">
					</tbody>
				</table>
			</div>
			 <sec:authorize ifAllGranted="ROLE_ADMIN">
				 <div class="browse-container">
						<div class="rounded-container drag-and-drop large panel" id="file-upload-drop-zone">
							<h1>Drop files here</h1>
							<h3>to upload them as additional channels</h3>
							<input id="fileData" type="file" name="fileData" data-url="/adobepass/api/upload" />
						</div>
						<div class="rounded-container panel small">
							<a id="browse-button">Browse</a>
						</div>
				 </div>
			 </sec:authorize>
		</div>
	</body>
</html>