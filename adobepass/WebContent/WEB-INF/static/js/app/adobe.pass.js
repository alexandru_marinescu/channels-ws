var AdobePass = AdobePass || {};

AdobePass.Launcher = (function(){
	var priceApiUrl = '/adobepass/api/price/';
	var channelsApiUrl = '/adobepass/api/channels/';
	var availableChannelsContainer = 'available-channels';
	var browseButton = 'browse-button';

	var toTimeStamp = function(dateStr) {
		var dateArray = dateStr.split('/');
		var year = dateArray[0];
		var month = dateArray[1];
		var day = dateArray[2];

		return Math.floor(+new Date(year, month, day, 0, 0, 0)/1000);
	};

	var attachInputFieldBehaviours = function () {
		var defaultValue = "YYYY/MM/DD";
		

		$.each(['#to-date', '#from-date'], function(index, item) {
			$(item).val(defaultValue).addClass("gray-info");
			$(item).focus(function () {
				if ($(this).val() == defaultValue) {
					$(this).val("");
					$(this).removeClass("gray-info");
				}
			}).blur(function() {
				if ($(this).val() == "") {
					$(this).val(defaultValue);
					$(this).addClass("gray-info");
				}
			});	
		});
	};

	var getPrice = function() {
		// TODO: validate data form.
		var start = toTimeStamp($('#from-date').val());
		var end = toTimeStamp($('#to-date').val());
		var occ = $('#occurences').val();
		var duration = $('#duration').val();

		$('#price-table-body').empty();
		
		$.getJSON(priceApiUrl + [start, end, occ, duration].join('/'), function (data) {

			$('#prices-container').show();
			$.each(data, function(index, item) {
				$('<tr><td>'+item['name']+'</td><td>'+item['price'] + " " + item['currency']+'</td></tr>')
						.appendTo('#price-table-body');
			});
		});
	};

	var getAvailableChannels = function(apiUrl, targetListId) {
		var apiUrl = channelsApiUrl;
		var targetListId = availableChannelsContainer;

		$.getJSON(apiUrl, function(data) {
				$('#'+targetListId).empty();
				var sortedData = data.sort(function (a, b) {
					return (a['name'] < b['name'] ? -1 : (a['name']>b['name'] ? 1 : 0));
				});

				$.each(sortedData, function(index, item) {
					$('<li><span class="channel-name">'+item['name']+'</span></li>').appendTo('#'+targetListId);
				});
		});
	};

	var attachFileUploader = function(inputId, dropZoneId) {
		var button = $('#'+browseButton);
		var inputFile = $('#'+inputId);

		button.addClass('button');
		button.click(function() {
			$('#fileData').click();
		});
		
		inputFile.fileupload({
			dropZone: $('#'+dropZoneId),
			dataType: 'json',
			done: function (e, data) {
				getAvailableChannels(channelsApiUrl, availableChannelsContainer);
				button.text("Browse");
				button.addClass("button");
				button.removeClass("channel-name");
			}
		});

		inputFile.change(function(evt) {
			button.text("Uploading ...");
			button.removeClass("button");
			button.addClass("channel-name");
			inputFile.fileupload('send', {files: evt.target.files});
		});

	};
	
	var attachBrowseButton = function() {
		
	}

	var attachGetPriceBehaviour = function() {
		$('#get-prices-button').click(function (e) {
			getPrice();
			e.preventDefault();
		});
	};
	
	return {
		attachBrowseButton: attachBrowseButton,
		attachFileUploader: attachFileUploader,
		attachGetPriceBehaviour: attachGetPriceBehaviour,
		attachInputFieldBehaviours : attachInputFieldBehaviours,
		getAvailableChannels: getAvailableChannels
	};
}());

$(document).ready(function () {
	AdobePass.Launcher.attachFileUploader('fileData', 'file-upload-drop-zone');
	AdobePass.Launcher.getAvailableChannels();
	AdobePass.Launcher.attachGetPriceBehaviour();
	AdobePass.Launcher.attachBrowseButton();
	AdobePass.Launcher.attachInputFieldBehaviours();
});