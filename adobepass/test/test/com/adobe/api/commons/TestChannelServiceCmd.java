package test.com.adobe.api.commons;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import com.adobe.pass.api.commons.ChannelServiceCmd;

public class TestChannelServiceCmd extends TestCase {

	public void testCmdCreation() {
		List<String> args = new ArrayList<String>();
		args.add("cmd-1");
		args.add("arg-1");
		args.add("arg-2");
		args.add("arg-3");

		ChannelServiceCmd cmd = new ChannelServiceCmd(args);
		assertEquals(cmd.getCmd(), "cmd-1");
		assertEquals(cmd.getArgs().size(), 3);
		assertEquals(cmd.getArgs().get(0), "arg-1");
	}
	
	public void testCmdVarArgs() {
		ChannelServiceCmd cmd = new ChannelServiceCmd("cmd-1", "arg-1", "arg-2", "arg-3");
		assertEquals(cmd.getCmd(), "cmd-1");
		assertEquals(cmd.getArgs().size(), 3);
	}
	
	public void testCmdVarNoArgs() {
		ChannelServiceCmd cmd = new ChannelServiceCmd("just-cmd");
		assertEquals(cmd.getCmd(), "just-cmd");
		assertEquals(cmd.getArgs().size(), 0);

		List<String> justCmd = new ArrayList<String>();
		justCmd.add("just-command");
		cmd = new ChannelServiceCmd(justCmd);

		assertEquals(cmd.getArgs().size(), 0);
	}
}
