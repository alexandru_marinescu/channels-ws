package test.com.adobe.api.commons;

import junit.framework.TestCase;

import com.adobe.pass.api.commons.ChannelServiceCmd;
import com.adobe.pass.api.commons.ChannelUtils;

public class TestChannelUtils extends TestCase {
	public void testSerialize() {
		ChannelServiceCmd cmd = new ChannelServiceCmd("cmd", "arg-1", "arg-2");
		byte[] serializedBytes = ChannelUtils.serializeObject(cmd);
		
		ChannelServiceCmd deserializedCmd = (ChannelServiceCmd) 
				ChannelUtils.deserializeObject(serializedBytes);

		assertEquals(cmd.getCmd(), deserializedCmd.getCmd());
		assertEquals(cmd.getArgs(), deserializedCmd.getArgs());
		
	}
}
