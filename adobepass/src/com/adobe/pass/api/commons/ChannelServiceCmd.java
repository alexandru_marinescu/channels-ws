package com.adobe.pass.api.commons;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Model for a command passed between front-end clients and backend workers.
 * 
 * A command can have a variable number of arguments ranging from from 0 to N.
 *
 * @author Alexandru Marinescu <almarinescu@gmail.com>
 */
public class ChannelServiceCmd implements Serializable {
	private static final long serialVersionUID = 6520273111910588993L;
	private String command;
	private List<String> args;

	public ChannelServiceCmd(List<String> cmd) {
		command = cmd.get(0);
		args = new ArrayList<String>();

		for (int i = 1; i < cmd.size(); i++) {
			args.add(cmd.get(i));
		}	
	}

	public ChannelServiceCmd(String cmd, String... argList) {
		this.command = cmd;
		args = new ArrayList<String>();

		for (String arg : argList) {
			args.add(arg);
		}
	}

	public List<String> getArgs() {
		return args;
	}

	public String getCmd() {
		return command;
	}
	
	public String toString() {
		return String.format("%s - %s", command, args);
	}

	public boolean equals(ChannelServiceCmd obj) {
		return obj.getCmd() == getCmd() && getArgs() == obj.getArgs();
	}
}
