package com.adobe.pass.api.commons;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.adobe.pass.api.backend.impl.zeromq.ChannelServiceServerWorker;

/**
 * Helper class which provides object serialization / deserialization mechanisms.
 * 
 * @author Alexandru Marinescu <almarinescu@gmail.com>
 *
 */
public class ChannelUtils {
	private static Log logger = LogFactory.getLog(ChannelServiceServerWorker.class);

	public static Object deserializeObject(byte[] objectBytes) {
		ObjectInputStream obj;
		Object resultingObj = null;

		try {
			obj = new ObjectInputStream(new ByteArrayInputStream(objectBytes));
			resultingObj = obj.readObject();
		} catch (ClassNotFoundException e) {
			logger.error(String.format("Failed to load command %s", e));
		} catch (IOException e) {
			logger.error(String.format("Failed to load command %s", e));
		}

		return resultingObj;
	}

	public static byte[] serializeObject(Object obj) {
		ByteArrayOutputStream baOut = new ByteArrayOutputStream();
		ObjectOutputStream out = null;

		try {
			out = new ObjectOutputStream(baOut);
			out.writeObject(obj);
			out.close();
		} catch (IOException e) {
			logger.error(String.format("Failed to serialize %s - %s", obj.toString(), e));
		}

		return baOut.toByteArray();
	}
}
