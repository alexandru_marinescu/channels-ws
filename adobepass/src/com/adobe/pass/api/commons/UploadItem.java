package com.adobe.pass.api.commons;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

/**
 * Bean for the jar upload form.
 *
 * @author Alexandru Marinescu <almarinescu@gmail.com>
 */
public class UploadItem
{
	private CommonsMultipartFile fileData;

	public CommonsMultipartFile getFileData() {
		return fileData;
	}

	public void setFileData(CommonsMultipartFile fileData) {
		this.fileData = fileData;
	}
}
