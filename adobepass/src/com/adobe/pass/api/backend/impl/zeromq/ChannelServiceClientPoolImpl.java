package com.adobe.pass.api.backend.impl.zeromq;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.adobe.pass.api.backend.interfaces.ChannelServiceClient;
import com.adobe.pass.api.backend.interfaces.ChannelServiceClientPool;

/**
 * A client pool implementation for the ChannelServiceClient based on the ZeroMQ communication.
 * 
 * This client pool preallocates a number of clients and stores them in a list of available clients.
 * When a client is acquired, a client is removed from the list of available clients and is moved
 * in a set of used clients. That client is only going to be made available when a release operation
 * is going to be executed on this queue.
 * 
 * The time complexity for an acquire operation is O(1) and the complexity for a release operation 
 * is O(lg(max-clients)).
 * 
 * @author Alexandru Marinescu <almarinescu@gmail.com>
 */
public class ChannelServiceClientPoolImpl implements ChannelServiceClientPool {
	private String serverAddress;
	private List<ChannelServiceClient> availableClients;
	private Set<ChannelServiceClient> usedClients;

	public ChannelServiceClientPoolImpl(String address, int maxClients) {
		serverAddress = address;
		availableClients = new ArrayList<ChannelServiceClient>();
		usedClients = new HashSet<ChannelServiceClient>();

		for (int i = 0; i < maxClients; i++) {
			availableClients.add(new ChannelServiceClientImpl(serverAddress));
		}
	}

	@Override
	public synchronized ChannelServiceClient acquireClient() {
		ChannelServiceClient client = availableClients.remove(availableClients.size() - 1);
		usedClients.add(client);
		return client;
	}

	@Override
	public synchronized void releaseClient(ChannelServiceClient client) {
		usedClients.remove(client);
		availableClients.add(client);
	}
}
