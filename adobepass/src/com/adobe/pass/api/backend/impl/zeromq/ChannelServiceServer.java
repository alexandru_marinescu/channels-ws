package com.adobe.pass.api.backend.impl.zeromq;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Context;
import org.zeromq.ZMQ.Poller;
import org.zeromq.ZMQ.Socket;

/**
 * An asynchronous server that routes requests received from web-clients to backend-workers.
 * 
 * When a request is received from a front-end client it gets routed in a round-robin manner to one
 * of the available workers. The server is non-blocking.
 * 
 * When a worker finishes processing a request, it sends the response back to the server alongside
 * with the identity of the client that requested that response. The server will route back the 
 * response to the client that requested it.
 * 
 * The connection between the front-end clients and the server can be made via tcp, whilst the
 * connection between the back-end workers and the asynchronous server can be made in process if the
 * workers are threads of the server or via tcp/ip if the workers are seperate processes.
 *
 * @author Alexandru Marinescu <almarinescu@gmail.com>
 */
public class ChannelServiceServer {
	private static Log logger = LogFactory.getLog(ChannelServiceServerWorker.class);
	private static int FRONTEND = 0;
	private static int NO_FLAGS = 0;
	private static int BACKEND = 1;

	private String frontEndAddress;
	private String backEndAddress;

	private Context serverContext;
	private Socket frontendConnection; 
	private Socket backendConnection;

	public ChannelServiceServer(String frontEndAddress, String backEndAddress) {
		this.frontEndAddress = frontEndAddress;
		this.backEndAddress = backEndAddress;
	}

	public void start() {
		serverContext = ZMQ.context(1);

		frontendConnection = serverContext.socket(ZMQ.ROUTER);
		frontendConnection.bind(frontEndAddress);

		backendConnection = serverContext.socket(ZMQ.DEALER);
		backendConnection.bind(backEndAddress);

		Poller poll = serverContext.poller();
		poll.register(frontendConnection, ZMQ.Poller.POLLIN);
		poll.register(backendConnection, ZMQ.Poller.POLLIN);

		while (true) {
			poll.poll();

			if (poll.pollin(FRONTEND)) {
				byte[] id = frontendConnection.recv(NO_FLAGS);
				byte[] msg = frontendConnection.recv(NO_FLAGS);

				logger.info("SERVER: Received msg to frontend: " + new String(msg));

				backendConnection.send(id, ZMQ.SNDMORE);
				backendConnection.send(msg, NO_FLAGS);
			}

			if (poll.pollin(BACKEND)) {
				byte[] id = backendConnection.recv(NO_FLAGS);
				byte[] msg = backendConnection.recv(NO_FLAGS);

				logger.info("SERVER: Sending msg to frontend: " + new String(msg));
				frontendConnection.send(id, ZMQ.SNDMORE);
				frontendConnection.send(msg, NO_FLAGS);
			}
		}
	}

	public void stop() {
		frontendConnection.close();
		backendConnection.close();
		serverContext.term();
	}
}
