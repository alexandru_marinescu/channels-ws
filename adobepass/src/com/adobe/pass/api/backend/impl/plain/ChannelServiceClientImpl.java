package com.adobe.pass.api.backend.impl.plain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;

import com.adobe.pass.api.backend.interfaces.ChannelLoader;
import com.adobe.pass.api.backend.interfaces.ChannelServiceClient;
import com.adobe.pass.api.commons.ChannelServiceCmd;
import com.adobe.pass.api.commons.ChannelUtils;
import com.adobe.pass.interfaces.CommercialPrice;
import com.adobe.pass.interfaces.TvChannelService;

/**
 * Provide the main API channel service.
 * 
 * This class is a plain implementation of the ChannelServiceClient interface.
 * Each instance of this class has a channel loader. 
 * 
 * This class allows querying for prices on all the available channels, get a list of available
 * channels and at the same time, load new channels and make them available for further usage.
 *
 * @author Alexandru Marinescu <almarinescu@gmail.com>
 */
public class ChannelServiceClientImpl implements ChannelServiceClient {
	public final static String KEY_CHANNELS = "channels";
	public final static String KEY_CURRENCY = "currency";
	public final static String KEY_ID = "id";
	public final static String KEY_NAME = "name";
	public final static String KEY_PRICE = "price";
	public final static String KEY_STATUS = "status";

	public final static String STATUS_OK = "OK";
	public final static String STATUS_FAIL = "FAIL";

	private ChannelLoader channelLoader;

	public ChannelServiceClientImpl(ChannelLoader loader) {
		channelLoader = loader;
	}

	/**
	 * Takes a filename and an array of bytes and uses the channel loader's storage provider to save
	 * the file.
	 * 
	 * @return {'status': 'OK'} - if the file is saved, {'status': 'FAIL'} - otherwise.
	 */
	@Override
	public Map<String, String> save(String filename, byte[] fileContents) {
		boolean fileSaved = channelLoader.getStorageProvider().save(filename, fileContents);
		channelLoader.refresh();

		Map<String, String> response = new HashMap<String, String>();

		if (!fileSaved) {
			response.put(KEY_STATUS, STATUS_FAIL);
		} else {
			response.put(KEY_STATUS, STATUS_OK);
		}

		return response;
	}

	/**
	 * Return a list of available channels.
	 * 
	 * This method returns a list which contains maps with two keys: name and id. The value for the
	 * 'id' key is the channel id, while the 'name' key holds the channel's name.
	 * 
	 * The id returned by this method for a given channel can be further used to query the service
	 * for a price for a particular channel, instead of prices for all the available channels.
	 */
	@Override
	public List<Map<String, String>> getAvailableChannels() {
		List<Map<String, String>> response = new ArrayList<Map<String, String>>();
		for (TvChannelService channel : channelLoader.getAvailableChannels()) {
			Map<String, String> channelHash = new HashMap<String, String>();
			channelHash.put(KEY_ID, channel.getId());
			channelHash.put(KEY_NAME, channel.getName());
			response.add(channelHash);
		}

		return response;
	}

	/**
	 * Compute the price for all available channels given the input parameters.
	 * 
	 * This method queries the channel loader for a list of available channels and then it iterates
	 * over this list to get the price for each channel.
	 * 
	 * This method returns a list of dictionaries. Each dictionary will have the following keys:
	 *   - id - the channel id.
	 *   - name - the channel name.
	 *   - price - the price for that particular channel with the given parameters as inputs.
	 *   - currency - the currency used to express the computed price for the given channel.
	 */
	@Override
	public List<Map<String, String>> getPrice(int start, int end,
			int duration, int occurences) {
		List<Map<String, String>> channels = new ArrayList<Map<String, String>>();

		for (TvChannelService channel : channelLoader.getAvailableChannels()) {
			Map<String, String> channelResult = new HashMap<String, String>();
			CommercialPrice price = channel.getPrice(start, end, duration, occurences);
			channelResult.put(KEY_ID, channel.getId());
			channelResult.put(KEY_NAME, channel.getName());
			channelResult.put(KEY_PRICE, new Float(price.getPrice()).toString());
			channelResult.put(KEY_CURRENCY, price.getCurrency());
			channels.add(channelResult);
		}

		return channels;
	}

	/**
	 * Compute the price for a given channel.
	 * 
	 * This method loads the required channel using the channel loader and the computes the price
	 * with the given parameters as input.
	 * 
	 * The result of this method is a dictionary with the same arguments as the dictionaries 
	 * returned for by the method that computes price of all channels.
	 */
	@Override
	public Map<String, String> getPrice(String channelId, int start, int end,
			int duration, int occurences) {
		TvChannelService channel = channelLoader.getChannel(channelId);
		Map<String, String> response = new HashMap<String, String>();

		if (channel == null) {
			response.put(KEY_STATUS, STATUS_FAIL);
		} else {
			CommercialPrice price = channel.getPrice(start, end, duration, occurences);
			response.put(KEY_ID, channel.getId());
			response.put(KEY_NAME, channel.getName());
			response.put(KEY_PRICE, new Float(price.getPrice()).toString());
			response.put(KEY_CURRENCY, price.getCurrency());
			response.put(KEY_STATUS, STATUS_OK);
		}

		return response;
	}

	/**
	 * This method is used as a RPC mechanism for calling methods defined by the raw interface.
	 * 
	 * This methods transforms the input bytes array into a ChannelServiceCmd and then parses it
	 * to extract its arguments and then call one of the public methods exposed by this class.
	 * 
	 * If a 'save' command is given with a Base64 encoded string, then the 'save' method is used.
	 * If a 'channels' command is given with no arguments, then the 'getAvailableChannels' method
	 * is called.
	 * If a 'price' command is given with 4 arguments, then the price for all available channels 
	 * is computed. If the 'price' command has 5 arguments, then the price for a given channel is 
	 * computed.
	 * 
	 * The result of this method is serialized back to an array of bytes.
	 * 
	 */
	@Override
	public byte[] processCommand(byte[] commandBytes) {
		ChannelServiceCmd command = (ChannelServiceCmd) ChannelUtils.deserializeObject(commandBytes);

		if (command.getCmd().equals("save")) {
			byte[] fileContents = Base64.decodeBase64(command.getArgs().get(1));
			Object result = save(command.getArgs().get(0), fileContents);
			return ChannelUtils.serializeObject(result);
		} else if (command.getCmd().equals("channels")) {
			Object result = getAvailableChannels();
			return ChannelUtils.serializeObject(result);
		} else if (command.getCmd().equals("price")) {
			List<String> cmdArgs = command.getArgs();
			if (cmdArgs.size() == 4) {
				Object result = getPrice(
						Integer.parseInt(cmdArgs.get(0)), Integer.parseInt(cmdArgs.get(1)), 
						Integer.parseInt(cmdArgs.get(2)), Integer.parseInt(cmdArgs.get(3)));
				return ChannelUtils.serializeObject(result);
			} else if (cmdArgs.size() == 5) {
				Object result = getPrice(
						cmdArgs.get(0),
						Integer.parseInt(cmdArgs.get(1)), Integer.parseInt(cmdArgs.get(2)), 
						Integer.parseInt(cmdArgs.get(3)), Integer.parseInt(cmdArgs.get(4)));
				return ChannelUtils.serializeObject(result);
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
}
