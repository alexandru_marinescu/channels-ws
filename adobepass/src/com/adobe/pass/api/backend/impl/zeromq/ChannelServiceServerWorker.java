package com.adobe.pass.api.backend.impl.zeromq;

import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Context;
import org.zeromq.ZMQ.Socket;

import com.adobe.pass.api.backend.interfaces.ChannelServiceClient;

/**
 * A backend worker for computing tv channel prices.
 * 
 * Each worker has a ChannelLoader. When a new message is received as an array of bytes, the 
 * message is passed along to the inner ChannelServiceClient for processing. The 
 * ChannelServiceClient uses it's processCommand method to convert the array of bytes to a 
 * ChannelServiceCmd which then is used to process the response.
 * 
 * The advantage of a worker is that it has it's own ChannelLoader backend by its oen storage 
 * provider. This means that in an architecture with multiple workers, more write requests can be 
 * satisfied than architectures that leverage the usage of a single storage provider.
 * 
 * While the message is compute, the worker is blocked. In the meanwhile, all the incoming requests
 * are queued for later processing.
 *
 * @author Alexandru Marinescu <almarinescu@gmail.com>
 */
public class ChannelServiceServerWorker implements Runnable {	
	private ChannelServiceClient channelClient;
	private String serverAddress;

	public ChannelServiceServerWorker(String address, ChannelServiceClient client) {
		channelClient = client;
		serverAddress = address;
	}

	@Override
	public void run() {
		Context ctx = ZMQ.context(1);
		Socket workerSocket = ctx.socket(ZMQ.DEALER);
		workerSocket.connect(serverAddress);

		while (true) {
			byte[] clientId = workerSocket.recv(0);
			byte[] commandBytes = workerSocket.recv(0);
			byte[] result = channelClient.processCommand(commandBytes);

			workerSocket.send(clientId, ZMQ.SNDMORE);
			workerSocket.send(result, 0);
		}
	}
}
