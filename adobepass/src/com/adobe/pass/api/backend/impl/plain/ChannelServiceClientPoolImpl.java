package com.adobe.pass.api.backend.impl.plain;

import com.adobe.pass.api.backend.interfaces.ChannelLoader;
import com.adobe.pass.api.backend.interfaces.ChannelServiceClient;
import com.adobe.pass.api.backend.interfaces.ChannelServiceClientPool;

/**
 * Client pool implementation for the plain ChannelServiceClient implementation.
 * 
 * This is a pseudo client pool since for each acquire operation, a new client is created.
 * The release operation has no impact over how resources are deallocated.
 * 
 * The purpose of this client pool is to provide a thin wrapper over the plain implementation of
 * a ChannelServiceClient. 
 * 
 * The result of using this implementation for a ChannelServiceClientPool is that the end-user is
 * going to receive a new client for each acquire operation.
 * 
 * One important aspect is that all clients created through this client pool share the same 
 * ChannelLoader instance.
 * 
 * @author Alexandru Marinescu <almarinescu@gmail.com>
 */
public class ChannelServiceClientPoolImpl implements ChannelServiceClientPool {
	private ChannelLoader channelLoader;
	private ChannelServiceClient client;

	public ChannelServiceClientPoolImpl(ChannelLoader loader) {
		channelLoader = loader;
		client = new ChannelServiceClientImpl(channelLoader);
	}

	@Override
	public ChannelServiceClient acquireClient() {
		return client;
	}

	@Override
	public void releaseClient(ChannelServiceClient client) {
	}
}
