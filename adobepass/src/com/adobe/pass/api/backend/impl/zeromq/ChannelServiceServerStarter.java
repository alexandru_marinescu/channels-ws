package com.adobe.pass.api.backend.impl.zeromq;

import java.util.ArrayList;
import java.util.List;

import com.adobe.pass.api.backend.impl.plain.ChannelServiceClientImpl;
import com.adobe.pass.api.backend.interfaces.ChannelLoader;
import com.adobe.pass.api.backend.interfaces.ChannelServiceClient;
import com.adobe.pass.api.backend.interfaces.StorageProvider;
import com.adobe.pass.api.backend.services.ChannelLoaderImpl;
import com.adobe.pass.api.backend.services.LocalStorageProviderImpl;

/**
 * A starter program for the ZeroMQ backend.
 * 
 * This method starts the asynchronous server, instantiate the back-end workers and creates the 
 * used ChannelLoaders used by the backend workers.
 * 
 * TODO(almarinescu): Create a configuration mechanism using one of the alternatives:
 *   - startup arguments.
 *   - property files.
 * 
 * @author Alexandru Marinescu <almarinescu@gmail.com>
 *
 */
public class ChannelServiceServerStarter {

	public static void main(String[] args) {
		int numWorkers = 1;
		String serverBindAddress = "tcp://*:5571";
		String serverConnectAddress = "tcp://localhost:5571";
		String storagePath = args[1]; //"C://Users//alex//Desktop";

		ChannelServiceServer server = new ChannelServiceServer("tcp://*:5570", serverBindAddress);

		List<Thread> workers = new ArrayList<Thread>();
		for (int i = 0; i < numWorkers; i++) {
			StorageProvider storage = new LocalStorageProviderImpl(storagePath);
			ChannelLoader loader = new ChannelLoaderImpl(storage);
			ChannelServiceClient client = new ChannelServiceClientImpl(loader);

			Thread worker = new Thread(new ChannelServiceServerWorker(serverConnectAddress, client));
			worker.start();
			workers.add(worker);
		}

		server.start();
	}
}
