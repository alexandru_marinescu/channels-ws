package com.adobe.pass.api.backend.impl.zeromq;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.codec.binary.Base64;
import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Context;
import org.zeromq.ZMQ.Poller;
import org.zeromq.ZMQ.Socket;

import com.adobe.pass.api.backend.interfaces.ChannelServiceClient;
import com.adobe.pass.api.commons.ChannelServiceCmd;
import com.adobe.pass.api.commons.ChannelUtils;

/**
 * Implementation for a ChannelServiceClient that forwards all API request to backend workers
 * using ZeroMQ as a transportation channel.
 * 
 * When a request comes to be processed by an instance of this implementation, the request is 
 * encapsulated in a ChannelServiceCmd, serializes it to a byte array and then sends it to an
 * asynchronous server.
 * 
 * After sending the serialized command, the client blocks until the response is received. The 
 * response is sent back from the server as an array of bytes which can be deserialized to the
 * same type as the return value of the method that's encapsulating the message communication.
 * 
 * TODO: 
 *  - Poll for a limited amount of time and retry for a limited number of times.
 *  - return empty message with failure status when no response is given.
 * 
 * @author Alexandru Marinescu <almarinescu@gmail.com>
 */
public class ChannelServiceClientImpl implements ChannelServiceClient {
	public static final String IDENTITY_PREFIX = "channel-service-client";
	public static final int NO_FLAGS = 0;

	public static final int NUM_RETRIES = 5;

	private int clientId;
	private Socket connection;
	private Context context;
	private Poller poller;

	public ChannelServiceClientImpl(String serverAddress) {
		// TODO(almarinescu): check if all clients can share the same connection context.
		clientId = new Random().nextInt();
		context = ZMQ.context(1);
		connection = context.socket(ZMQ.DEALER);

		connection.setIdentity(String.format("%s-%s", IDENTITY_PREFIX, clientId).getBytes());
		connection.connect(serverAddress);

		poller = context.poller(2);
		poller.register(connection, ZMQ.Poller.POLLIN);
	}

	private Object getResponse(ChannelServiceCmd cmd) {
		Object result = null;

		connection.send(ChannelUtils.serializeObject(cmd), NO_FLAGS);
		poller.poll();
		if (poller.pollin(0)) {
			byte[] responseBytes = connection.recv(NO_FLAGS);
			result = ChannelUtils.deserializeObject(responseBytes);
		}

		return result;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Map<String, String> save(String filename, byte[] fileContents) {
		ChannelServiceCmd saveCmd = new ChannelServiceCmd("save", filename, 
				Base64.encodeBase64String(fileContents));
		Map<String, String> response = (HashMap<String, String>) getResponse(saveCmd);

		return response;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Map<String, String>> getAvailableChannels() {
		ChannelServiceCmd channelsCmd = new ChannelServiceCmd("channels");
		List<Map<String, String>> response = (List<Map<String, String>>) getResponse(channelsCmd);
		return response;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Map<String, String>> getPrice(int start, int end,
			int duration, int occurences) {
		ChannelServiceCmd priceCmd = new ChannelServiceCmd("price", 
				new Integer(start).toString(), new Integer(end).toString(),
				new Integer(duration).toString(), new Integer(occurences).toString());
		List<Map<String, String>> response = (List<Map<String, String>>) getResponse(priceCmd);
		return response;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Map<String, String> getPrice(String channelId, int start, int end, 
			int duration, int occurences) {
		ChannelServiceCmd priceCmd = new ChannelServiceCmd("price", channelId,
				new Integer(start).toString(), new Integer(end).toString(),
				new Integer(duration).toString(), new Integer(occurences).toString());
		Map<String, String> response = (Map<String, String>) getResponse(priceCmd);
		return response;
	}

	@Override
	public byte[] processCommand(byte[] command) {
		return ChannelUtils.serializeObject(
				getResponse(
						(ChannelServiceCmd)ChannelUtils.deserializeObject(command)));
	}
}
