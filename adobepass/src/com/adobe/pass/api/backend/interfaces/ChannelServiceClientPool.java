package com.adobe.pass.api.backend.interfaces;

import com.adobe.pass.api.backend.interfaces.ChannelServiceClient;

/**
 * An interface for a client pool. It's a similar concept to a connection pool.
 * 
 * A set of ChannelServiceClients are created ahead of time and recycled in order to reduce the 
 * cost of creating new clients.
 * 
 * @author Alexandru Marinescu <almarinescu@gmail.com>
 *
 */
public interface ChannelServiceClientPool {
	public ChannelServiceClient acquireClient();
	public void releaseClient(ChannelServiceClient client);
}
