package com.adobe.pass.api.backend.interfaces;

import java.util.List;

import com.adobe.pass.interfaces.TvChannelService;

/**
 * A channel loader has the purpose of providing information about the available channels.
 * 
 * The channel loader is responsible for dynamically loading the list of available classes that 
 * implement the 'TvChannelService' interface.
 * 
 * All the read / write operations executed by a ChannelLoader are executed by its underlying 
 * StorageProvider.
 * 
 * @author Alexandru Marinescu <almarinescu@gmail.com>
 *
 */
public interface ChannelLoader {

	/**
	 * Return a list of instances for each of the available classes that implement the 
	 * TvChannelService interface.
	 */
	public List<TvChannelService> getAvailableChannels();
	
	/**
	 * Return an instance of a class that implements a TvChannelService and that corresponds to a 
	 * tv channel with the given channel id.
	 */
	public TvChannelService getChannel(String channelId);

	/**
	 * Return a reference to the backing storage provider of this channel loader.
	 */
	public StorageProvider getStorageProvider();
	
	/**
	 * When a new class is saved by the storage provider, the ServiceLoader used to load the 
	 * available tv channels is refreshed.
	 */
	public void refresh();
}
