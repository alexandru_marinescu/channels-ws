package com.adobe.pass.api.backend.interfaces;

import java.io.File;
import java.util.List;

/**
 * Provide services for storing channel services.
 *
 * @author Alexandru Marinescu <almarinescu@gmail.com>
 */
public interface StorageProvider {

	/**
	 * Create a file and write it's contents.
	 * 
	 * @param filename - target file name.
	 * @param contents - the information written in the target file.
	 * @return true if the file was successfully written
	 */
	public boolean save(String filename, byte[] contents);
	
	/** Return a list of all available files through this storage provider. */
	public List<File> getAvailableFiles();
}
