package com.adobe.pass.api.backend.interfaces;

import java.util.List;
import java.util.Map;

/**
 * Save new TvChannelServices and query existing ones.
 * 
 * This class is an abstraction over the channel loader and incorporates the necessary means to
 * do price queries and list all the available tv channels.
 * 
 * @author Alexandru Marinescu <almarinescu@gmail.com>
 */
public interface ChannelServiceClient {
	/**
	 * Save a new jar to a location specified by the underlying storage provider.
	 */
	public Map<String, String> save(String filename, byte[] fileContents);

	/**
	 * Returns a list of all the available channels with their names and their ids.
	 */
	public List<Map<String, String>> getAvailableChannels();

	/**
	 * Compute the price for all the available channels.
	 * 
	 * Start and end are UNIX timestamps.
	 */
	public List<Map<String, String>> getPrice(int start, int end, int duration, int occurences);

	/**
	 * Compute the price for the channel with the given channel id.
	 */
	public Map<String, String> getPrice( String channelId, int start, int end, int duration, 
			int occurences);

	/**
	 * Convert the input byte array to a ChannelServiceCmd and identify one of the methods that 
	 * need to be executed.
	 * 
	 * This method is used as a RPC mechanism. The result of the processing phase of the 
	 * ChannelServiceCmd is then serialized back to an array of bytes.
	 */
	public byte[] processCommand(byte[] command);
}
