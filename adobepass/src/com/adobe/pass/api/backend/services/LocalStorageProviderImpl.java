package com.adobe.pass.api.backend.services;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.adobe.pass.api.backend.interfaces.StorageProvider;

/**
 * This storage provider saves the .jar files on a file system location.
 * 
 * When it has to return a list of available files managed by this storage provider, it will scan 
 * the location it's managing for all the files that have .jar extension.
 *
 * @author Alexandru Marinescu <almarinescu@gmail.com>
 */
public class LocalStorageProviderImpl implements StorageProvider {
	private final File storagePath;
	private final Log storageLogger = LogFactory.getLog(LocalStorageProviderImpl.class); 

	public LocalStorageProviderImpl(String path) {
		storagePath = new File(path);
	}

	@Override
	public boolean save(String filename, byte[] contents) {
		FileOutputStream targetFile = null;
		String destPath = new File(storagePath, filename).getAbsolutePath();

		try {
			targetFile = new FileOutputStream(new File(storagePath, filename));
		} catch (FileNotFoundException e) {
			storageLogger.error(String.format("Unable to save %s in %s - %s", filename, destPath, e));
			return false;
		}

		try {
			targetFile.write(contents);
			targetFile.close();
		} catch (IOException e) {
			storageLogger.error(String.format("Unable to write %s bytes in %s - %s", 
					contents.length, destPath, e));
			return false;
		}

		return true;
	}

	@Override
	public List<File> getAvailableFiles() {
		List<File> result = new ArrayList<File>();
		
		for (File storedFile : storagePath.listFiles()) {
			if (storedFile.getName().endsWith("jar")) {
				result.add(storedFile);
			}
		}

		return result;
	}
}
