package com.adobe.pass.api.backend.services;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;

import com.adobe.pass.api.backend.interfaces.ChannelLoader;
import com.adobe.pass.api.backend.interfaces.StorageProvider;
import com.adobe.pass.interfaces.TvChannelService;

/**
 * Responsible for loading services from a service deployment directory.
 * 
 * Channel services are dynamically loaded from a given directory.
 * The deployment directory for a channel service loader can be changed after
 * its creation. The ServiceLoader instance used by this class is updated 
 * on each channel services query.
 * 
 * This class doesn't implement any kind of caching for the loaded classes other
 * than the caching offered out of the box by the ServiceLoader class.
 *
 * @author Alexandru Marinescu <almarinescu@gmail.com>
 */
public class ChannelLoaderImpl implements ChannelLoader {
	private ServiceLoader<TvChannelService> serviceLoader;
	private Map<String, TvChannelService> availableChannels;
	private StorageProvider storage;

	public ChannelLoaderImpl(StorageProvider sp) {
		storage = sp;
		serviceLoader = ServiceLoader.load(TvChannelService.class);
		refresh();
	}

	public void refresh() {
		extendClassPath(storage.getAvailableFiles());
		serviceLoader.reload();
	}

	@Override
	public StorageProvider getStorageProvider() {
		return storage;
	}

	@Override
	public List<TvChannelService> getAvailableChannels() {
		refresh();

		List<TvChannelService> services = new ArrayList<TvChannelService>();
		availableChannels = new HashMap<String, TvChannelService>();

		for (TvChannelService channel : serviceLoader) {
			availableChannels.put(channel.getId(), channel);
			services.add(channel);
		}

		return services;
	}

	@Override
	public TvChannelService getChannel(String channelId) {
		if (availableChannels == null) {
			getAvailableChannels();
		}

		return availableChannels.get(channelId);
	}

	private void extendClassPath(List<File> jars) {
		for (File jarFile : jars) {
			try {
				loadJarInClassPath(jarFile);
			} catch (Exception ex) {
				throw new IllegalArgumentException("Unable to load JAR: " + jarFile, ex);
			}
		}
	}

	/**
	 * In order to allow the ChannelLoader to include jars which aren't necessary in the CLASSPATH,
	 * we expose a private method of this class class loader to add a jar pointed by an URL.
	 */
	private Object loadJarInClassPath(File jarFile) throws 
			NoSuchMethodException, SecurityException, 
			IllegalAccessException, IllegalArgumentException, 
			InvocationTargetException, MalformedURLException {
		Method m = URLClassLoader.class.getDeclaredMethod("addURL", new Class[]{URL.class});
		m.setAccessible(true);
		return m.invoke(
				Thread.currentThread().getContextClassLoader(),
				new Object[]{jarFile.toURI().toURL()});
	}

}
