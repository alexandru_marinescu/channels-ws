package com.adobe.pass.api.cache;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Callable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.adobe.pass.api.backend.interfaces.ChannelServiceClient;
import com.adobe.pass.api.backend.interfaces.ChannelServiceClientPool;
import com.adobe.pass.api.commons.ChannelUtils;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

/**
 * Cache API responses for requests that target any of the resources served by the given list of 
 * URL providers.
 * 
 * The channel service cache uses a client pool to generate a client instance which it's going 
 * to use it to generate responses that are going to be stored in the cache.
 * 
 * The cache is using as a key the URL of the incoming http requests.
 * 
 * The cache gets invalidated once there is a request for a resource that it's marked as a resource
 * that evicts / invalidates the cache.
 * 
 * @author Alexandru Marinescu <almarinescu@gmail.com>
 */
public class CacheInterceptor extends HandlerInterceptorAdapter {
	private ChannelServiceClient client;
	private Cache<String, String> jsonCache;
	private ObjectMapper jsonMapper;
	private List<CachingUrlProvider> urlProviders;
	
	private Log logger = LogFactory.getLog(CacheInterceptor.class);

	public CacheInterceptor(ChannelServiceClientPool pool, List<CachingUrlProvider> providers) {
		client = pool.acquireClient();
		jsonCache = CacheBuilder.newBuilder()
	       .maximumSize(1000)
	       .recordStats()
	       .build();
		jsonMapper = new ObjectMapper();
		urlProviders = providers;
	}

	/**
	 * Given a URI, identify the target API end point, extract the parameters from the URI, call the
	 * end-point with the proper arguments and convert the result to a JSON representation 
	 * serialized as a String.
	 * 
	 * @return a json representation of the API end-point pointed by the given URL or the empty 
	 * string in case no end-point gets identified.
	 */
	private String httpRequestToJson(HttpServletRequest request) {
		CacheUrlEntry matchingEntry = getMatchingEntry(request.getRequestURI());
		if (matchingEntry == null) {
			return null;
		}

		return toJson(
				ChannelUtils.deserializeObject(
						client.processCommand(
								ChannelUtils.serializeObject(
										matchingEntry.getServiceCmd(request.getRequestURI())))));
	}

	private CacheUrlEntry getMatchingEntry(String uri) {
		return CachingUrlProvider.getMatchingEntry(uri, urlProviders);
	}

	/** Convert a Java object to a JSON representation serialized as a java.lang.String. */
	private String toJson(Object data) {
		String result = null;
		try {
				result = jsonMapper.writeValueAsString(data);
		} catch (JsonGenerationException e) {
		} catch (JsonMappingException e) {
		} catch (IOException e) {
		}

		return result;
	}


	/** Return a cache-key from a http request. */
	private String cacheKey(HttpServletRequest request) {
		return request.getRequestURI();
	}

	/**
	 * Before the handler gets to be executed, check if there is a cache entry and return it, if 
	 * there is one.
	 * 
	 * If the current request is targeting the a cache invalidating API end-point, this method 
	 * invalidates the cache.
	 * 
	 * If there is a cache entry for the current request, then stop propaging the request down the 
	 * handler chain and return the JSON response from the json cache.
	 * 
	 * If we don't have a cache hit, the request is forwarded to the rest of the handler chain as
	 * usual.
	 */
	@Override
	public boolean preHandle(final HttpServletRequest request, HttpServletResponse response,
			Object handler) throws Exception {
		CacheUrlEntry matchingEntry = getMatchingEntry(request.getRequestURI());
		logger.info(jsonCache.stats().toString());

		// Current request isn't matched by any of the available URL providers.
		if (matchingEntry == null) {
			return true;
		}

		if (matchingEntry.isInvalidatingCache()) {
			jsonCache.invalidateAll();
		}

		if (!matchingEntry.isCacheable()) {
			return true;
		} else {
			if (jsonCache.getIfPresent(cacheKey(request)) != null) {
				response.getWriter().write(jsonCache.get(cacheKey(request), new Callable<String>() {
			    @Override public String call() {
			      return httpRequestToJson(request);
			    }
			  }));
				response.setContentType("application/json");
				return false;
			}	else {
				return true;
			}
		}
	}

	/**
	 * Cache the response of the current request if necessary.
	 * 
	 * After the request has finished processing, if the request is cacheable and if there isn't a 
	 * cache hit for the current request, then we cache the API response for that request.
	 */
	@Override
	public void afterCompletion(
			HttpServletRequest request, HttpServletResponse response,
			Object handler, Exception exception) {
		CacheUrlEntry matchingEntry = getMatchingEntry(request.getRequestURI());

		if (matchingEntry != null && matchingEntry.isCacheable()) {
			logger.info(cacheKey(request) + "-> " + httpRequestToJson(request));

			if (jsonCache.getIfPresent(cacheKey(request)) == null) {
				jsonCache.put(cacheKey(request), httpRequestToJson(request));
			}
		}
	}
}
