package com.adobe.pass.api.cache;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * A method annotated with this annotation will invalidated the global cache.
 * 
 * @author Alexandru Marinescu <almarinescu@gmail.com>
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface InvalidateCache {}
