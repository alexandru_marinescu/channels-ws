package com.adobe.pass.api.cache;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.StringUtils;

import com.adobe.pass.api.commons.ChannelServiceCmd;

public class CacheUrlEntry {
	public static class CacheUrlEntryBuilder {
		private CacheUrlEntry instance;
		public CacheUrlEntryBuilder() {
			instance = new CacheUrlEntry();
		}
		
		
		public boolean hasUri() {
			return instance.getUrl() != null;
		}
		
		public boolean hasCacheProperty() {
			return instance.isCacheable != null;
		}
		
		public boolean hasEvictingProperty() {
			return instance.isEvictingCache != null;
		}

		public CacheUrlEntryBuilder setUri(String uri) {
			instance.setUri(uri);
			
			return this;
		}

		public CacheUrlEntryBuilder setControllerPrefix(String prefix) {
			instance.setControllerPrefix(prefix);
			
			return this;
		}
		
		public CacheUrlEntryBuilder setIsCacheable(boolean value) {
			instance.setIsCacheable(value);
			
			return this;
		}
		
		public CacheUrlEntryBuilder setIsInvalidatingCache(boolean value) {
			instance.setIsEvictingCache(value);
			
			return this;
		}
		
		public CacheUrlEntry build() {
			if (instance.isCacheable == null) {
				instance.isCacheable = false;
			}
			
			if (instance.isEvictingCache == null) {
				instance.isEvictingCache = false;
			}
			return instance;
		}
	}

	private String springUri;
	private String controllerPrefix;
	private Boolean isCacheable;
	private Boolean isEvictingCache;

	private CacheUrlEntry() {}

	public String getUrl() {
		return springUri;
	}
	
	private void setUri(String uri) {
		springUri = uri;
	}

	public ChannelServiceCmd getServiceCmd(String url) {
		List<String> args = extractCmdAndArgs(url);
		return new ChannelServiceCmd(args);
	}

	public Boolean isCacheable() {
		return isCacheable;
	}

	public void setIsCacheable(boolean value) {
		isCacheable = value;
	}

	private String getControllerPrefix() {
		return controllerPrefix;
	}

	private void setControllerPrefix(String value) {
		controllerPrefix = value;
	}

	public Boolean isInvalidatingCache() {
		return isEvictingCache;
	}

	private void setIsEvictingCache(boolean value) {
		isEvictingCache = value;
	}

	private String sanitize(String url) {
		String ctrlPrefix = getControllerPrefix();

		String sanitized = url.substring(url.indexOf(ctrlPrefix) + ctrlPrefix.length());
		sanitized = StringUtils.trimLeadingCharacter(sanitized, '/');
		sanitized = StringUtils.trimTrailingCharacter(sanitized, '/');

		return sanitized;
	}

	private List<String> extractCmdAndArgs(String url) {
		List<String> args = new ArrayList<String>();

		String inputUrl = sanitize(url);
		String innerUrl = sanitize(getUrl());

		String[] inputUrlChunks = inputUrl.split("/");
		String[] innerUrlChunks = innerUrl.split("/");

		if (inputUrlChunks.length != innerUrlChunks.length) {
			return null;
		}

		// the first element of the result is going to be the command name.
		args.add(inputUrlChunks[0]);

		for (int i = 1; i < inputUrlChunks.length; i++) {
			if (innerUrlChunks[i].startsWith("{")) {
				args.add(inputUrlChunks[i]);
				continue;
			}

			if (!innerUrlChunks[i].equals(inputUrlChunks[i])) {
				return null;
			}
		}

		return args;
	}
	
	public boolean matches(String url) {
		String ctrlPrefix = getControllerPrefix();
		if (url.indexOf(ctrlPrefix) == -1) {
			return false;
		}

		String inputUrl = sanitize(url);
		String innerUrl = sanitize(getUrl());
		
		String[] inputUrlChunks = inputUrl.split("/");
		String[] innerUrlChunks = innerUrl.split("/");

		if (inputUrlChunks.length != innerUrlChunks.length) {
			return false;
		}

		for (int i = 0; i < inputUrlChunks.length; i++) {
			if (innerUrlChunks[i].startsWith("{")) {
				continue;
			}

			if (!innerUrlChunks[i].equals(inputUrlChunks[i])) {
				return false;
			}			
		}

		return true;
	}

	public String toString() {
		return getUrl()	+ ", Cacheable: " + isCacheable + ", Evicting Cache:" + isEvictingCache;
	}
}
