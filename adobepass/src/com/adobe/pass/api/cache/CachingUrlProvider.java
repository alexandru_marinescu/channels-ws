package com.adobe.pass.api.cache;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Inspect a controller class, create a list of all methods available in that controller that have 
 * a RequestMapping annotation and create entries for each one of them.
 * 
 * Each entry will have information about its relation with the global cache (if the method is 
 * cached or not and if it should invalidate the cache, once a request for that method is received).
 * 
 * @author Alexandru Marinescu <almarinescu@gmail.com>
 *
 */
public class CachingUrlProvider {
	private Log logger = LogFactory.getLog(CachingUrlProvider.class);

	private String controllerClassName;
	private List<CacheUrlEntry> urlEntries;

	public CachingUrlProvider(String className) {
		controllerClassName = className;
		urlEntries = getUrlEntries();
	}

	@SuppressWarnings("rawtypes")
	private String getControllerUrlPrefix(String className) {
		Class aClass = null;
		try {
			aClass = CachingUrlProvider.class.getClassLoader().loadClass(className);
		} catch (ClassNotFoundException e) {}

		String mainUrl="";

		Annotation[] annotations = aClass.getAnnotations();

		for(Annotation annotation : annotations){
		    if(annotation instanceof RequestMapping){
		    	RequestMapping myAnnotation = (RequestMapping) annotation;
		    	mainUrl = myAnnotation.value()[0];
		    }
		}

		return mainUrl;
	}

	@SuppressWarnings("rawtypes")
	private List<CacheUrlEntry> getUrlEntries() {
		Class c = null;

		try {
			c = Class.forName(controllerClassName);
		} catch (ClassNotFoundException e) {
			logger.error("Unable to find class for " + controllerClassName + " - " + e);
		}

		if (c == null) {
			return null;
		}

		List<CacheUrlEntry> entries = new ArrayList<CacheUrlEntry>();
		String controllerPrefix = getControllerUrlPrefix(controllerClassName);

        for (Method m : c.getDeclaredMethods()) {
        	CacheUrlEntry.CacheUrlEntryBuilder urlEntryBuilder = 
        			new CacheUrlEntry.CacheUrlEntryBuilder();
        	urlEntryBuilder.setControllerPrefix(controllerPrefix);

        	for(Annotation annotation : m.getAnnotations()) {
    		    if(annotation instanceof RequestMapping){
    		    	RequestMapping myAnnotation = (RequestMapping) annotation;
    		    	urlEntryBuilder.setUri(controllerPrefix + myAnnotation.value()[0]);
    		    }
    		    
    		    if(annotation instanceof StoreInCache){
    		    	urlEntryBuilder.setIsCacheable(true);
    		    }
    		    
    		    if(annotation instanceof InvalidateCache){
    		    	urlEntryBuilder.setIsInvalidatingCache(true);
    		    }
    		}
        	
        	if (urlEntryBuilder.hasUri() 
        		&& (urlEntryBuilder.hasCacheProperty() || urlEntryBuilder.hasEvictingProperty())) {
        		entries.add(urlEntryBuilder.build());
        	}
        }

        logger.info(entries);
		return entries;
	}

	/**
	 * Return the UrlEntry that matches the given url.
	 */
	public CacheUrlEntry getMatchingEntry(String url) {
		for (CacheUrlEntry entry : urlEntries) {
			if (entry.matches(url)) {
				return entry;
			}
		}

		return null;
	}

	/**
	 * Return the matching UrlEntry from a list of UrlProviders.
	 */
	public static CacheUrlEntry getMatchingEntry(String url, List<CachingUrlProvider> providers) {
		for (CachingUrlProvider provider : providers) {
			CacheUrlEntry match = provider.getMatchingEntry(url); 
			if (match != null) {
				return match;
			}
		}

		return null;
	}
}
