package com.adobe.pass.api.frontend.controllers;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.adobe.pass.api.backend.interfaces.ChannelServiceClient;
import com.adobe.pass.api.backend.interfaces.ChannelServiceClientPool;
import com.adobe.pass.api.cache.InvalidateCache;
import com.adobe.pass.api.cache.StoreInCache;
import com.adobe.pass.api.commons.UploadItem;

/**
 * API controller.
 * 
 * @author Alexandru Marinescu <almarinescu@gmail.com>
 */
@Controller
@RequestMapping("/api")
public class ApiController {
	@Autowired
	private ChannelServiceClientPool clientPool;
	
	Log logger = LogFactory.getLog(ApiController.class);

	@InvalidateCache
	@RequestMapping(value="/upload", method=RequestMethod.POST) 
	public @ResponseBody Map<String, String> upload(
			UploadItem uploadItem, BindingResult bindingResult) {
		String uploadFileName = uploadItem.getFileData().getFileItem().getName();
		byte[] uploadFileBytes = uploadItem.getFileData().getBytes();

		ChannelServiceClient client = clientPool.acquireClient();
		Map<String, String> response = client.save(uploadFileName, uploadFileBytes);
		clientPool.releaseClient(client);

		return response;
	}

	@StoreInCache
	@RequestMapping(value="/channels", method=RequestMethod.GET)
	public @ResponseBody List<Map<String, String>> channels() {
		ChannelServiceClient client = clientPool.acquireClient();
		List<Map<String, String>> result = client.getAvailableChannels();
		clientPool.releaseClient(client);

		return result;
	}

	@StoreInCache
	@RequestMapping(value="/price/{start}/{end}/{duration}/{occurences}")
	public @ResponseBody List<? extends Object> allChannelsPrices(
			@PathVariable int start, @PathVariable int end,
			@PathVariable int duration, @PathVariable int occurences) {
		ChannelServiceClient client = clientPool.acquireClient();
		List<Map<String, String>> response = 
				client.getPrice(start, end, duration, occurences);
		clientPool.releaseClient(client);

		return response;
	}

	@StoreInCache
	@RequestMapping(value="/price/{channelId}/{start}/{end}/{duration}/{occurences}")
	public @ResponseBody Map<String, String> channelPrice(
			@PathVariable String channelId,
			@PathVariable int start, @PathVariable int end,
			@PathVariable int duration, @PathVariable int occurences) {
		ChannelServiceClient client = clientPool.acquireClient();
		Map<String, String> response = client.getPrice(channelId, start, end, duration, occurences);
		clientPool.releaseClient(client);

		return response;
	}
}
