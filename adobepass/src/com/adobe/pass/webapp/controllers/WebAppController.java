package com.adobe.pass.webapp.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Main controller.
 * 
 * All frontend request get routed to this controller.
 *
 * @author Alexandru Marinescu <almarinescu@gmail.com>
 */
@Controller
public class WebAppController {
	@RequestMapping(value = "/")
	public String index(Model model) {
		return "front.end.index";
	}
}
